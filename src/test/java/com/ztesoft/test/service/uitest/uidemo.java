package com.ztesoft.test.service.uitest;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;

import com.ztesoft.ip.utils.LayoutUtils;
import com.ztesoft.ip.utils.layout.SWTResourceManager;

import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.StyledText;

public class uidemo {

	protected Shell shell;
	protected static Composite shellcomp = null;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			uidemo window = new uidemo();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();

		shell = new Shell(0);
		shell.setSize(681, 468);
		shell.setText("SWT Application");
		shell.setImage(SWTResourceManager.getImage(ico.class, "/pictures/png-0004.png"));
		shellcomp = LayoutUtils.centerDWdefult(shell, "demo", true, true);
		shell.setLayout(new FormLayout());

		Composite composite = new Composite(shellcomp, SWT.NONE);
		composite.setLayout(new FormLayout());
		FormData fd_composite = new FormData();
		fd_composite.right = new FormAttachment(100);
		fd_composite.top = new FormAttachment(0);
		fd_composite.left = new FormAttachment(0);
		fd_composite.bottom = new FormAttachment(100);
		composite.setLayoutData(fd_composite);

		Composite composite_1 = new Composite(composite, SWT.NONE);
		composite_1.setBackground(SWTResourceManager.getColor(SWT.COLOR_CYAN));
		composite_1.setLayout(null);
		FormData fd_composite_1 = new FormData();
		fd_composite_1.bottom = new FormAttachment(0, 43);
		fd_composite_1.right = new FormAttachment(100);
		fd_composite_1.top = new FormAttachment(0);
		fd_composite_1.left = new FormAttachment(0);
		composite_1.setLayoutData(fd_composite_1);

		Composite composite_2 = new Composite(composite, SWT.NONE);
		composite_2.setBackground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		FormData fd_composite_2 = new FormData();
		fd_composite_2.right = new FormAttachment(100);
		fd_composite_2.top = new FormAttachment(composite_1);

		Label lblUiDemo = new Label(composite_1, SWT.NONE);
		lblUiDemo.setAlignment(SWT.CENTER);
		lblUiDemo.setBackground(SWTResourceManager.getColor(SWT.COLOR_CYAN));
		lblUiDemo.setBounds(285, 14, 61, 17);
		lblUiDemo.setText("ui demo");
		composite_2.setLayout(new FillLayout(SWT.HORIZONTAL));
		fd_composite_2.left = new FormAttachment(0);
		composite_2.setLayoutData(fd_composite_2);
		
		StyledText styledText = new StyledText(composite_2, SWT.BORDER);
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
}
