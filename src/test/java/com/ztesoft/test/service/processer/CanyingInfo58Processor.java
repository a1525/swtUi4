package com.ztesoft.test.service.processer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import com.alibaba.fastjson.JSON;
import com.jfinal.kit.HttpKit;
import com.ztesoft.ip.entity.BaiduMap;
import com.ztesoft.ip.utils.PropertiesUtil;

/**
 * @author code4crafter@gmail.com <br>
 */
public class CanyingInfo58Processor implements PageProcessor {

	public Integer id = 57;
	public static StringBuffer allsql = new StringBuffer("");
	public static final String URL_LIST = "http://yinchuan\\.58\\.com/jiefdj/shenghuo/.";

	public static final String URL_POST = "http://yinchuan\\.58\\.com/kaisuo/\\w+\\.shtml?\\w+";

	private Site site = Site.me().setDomain("58.com").setSleepTime(3000).setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

	@Override
	public void process(Page page) {
		// 列表页
		if (page.getUrl().regex(URL_LIST).match()) {
			// System.out.println(1);
			page.addTargetRequests(page.getHtml().xpath("//table[@class=\"tblist\"]").links().regex(URL_POST).all());
			page.addTargetRequests(page.getHtml().links().regex(URL_LIST).all());
			// 文章页
		} else {
			String name = page.getHtml().xpath("//div[@class='mainTitle']/h1/text()").toString();
			// System.out.println(name);
			String address = page.getHtml().xpath("//div[@class='userinfo']//ul[@class='uinfolist']//li[4]//p/text()").toString();

			// System.out.println(address);

			String localtion = getLocation(address);
			// System.out.println(localtion);
			String phone = page.getHtml().xpath("//span[@class='l_phone']/text()").get();
			// System.out.println(phone);

			makeSQL(name, address, localtion, phone);
		}
	}

	private void makeSQL(String name, String address, String localtion, String phone) {
		// TODO Auto-generated method stub
		int chanelid = 18;
		int areaid = 36;
		String sql = "insert into ccs_shop(shop_id,shop_channel_id,shop_name,area_id," + "shop_address,contact_phone,state, creator ,create_date,shop_level," + "consum_level,poi_point) values(" + (id++) + "," + chanelid + ",'" + name + "'," + areaid + ",'" + address + "','" + phone + "','A',2,to_date" + "('2015-07-08 11:46:00','yyyy-mm-dd hh24:mi:ss'),1,0.00,'" + localtion + "');";

		allsql.append(sql + "\n");

	}

	public static String getLocation(String address) {
		// TODO Auto-generated method stub
		PropertiesUtil.load("resouce/config", "value.config");

		String baidu_address = "http://api.map.baidu.com/place/v2/search?ak=" + PropertiesUtil.getProperty("baiak") + "" + "&output=json&query=" + address + "&page_size=1&page_num=0&scope=1&region=" + "银川";
		// System.out.println(baidu_address);
		BaiduMap map = JSON.parseObject(HttpKit.get(baidu_address), BaiduMap.class);
		if (null == map || null == map.getResults() || map.getResults().size() < 1 || null == map.getResults().get(0).getLocation()) {
			return "未找到坐标";
		}
		return map.getResults().get(0).getLocation().getInfo();

	}

	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		/*
		 * System.out.println("58 华新周边 美食启动："); Spider.create(new
		 * CanyingInfo58Processor()).addUrl(
		 * "http://yinchuan.58.com/jiefdj/shenghuo/?key=%E5%BC%80%E9%94%81&ampcmcskey=%E6%9C%88%E5%AB%82&ampfinal=1&ampspecialtype=gls&nearby=jiefdj&PGTID=163286953188173290720248650&ClickID=1"
		 * ).run(); System.out.println(allsql.toString());
		 */
		List<String> s = read();
		print(s);
	}

	private static List<String> read() {
		List<String> s = new ArrayList<String>();
		try {
			String encoding = "UTF-8";
			//String encoding = "gbk";
			File file = new File("resouce/config/add2.txt");
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);// 考虑到编码格式
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {
					//System.out.println(lineTxt);
					s.add(lineTxt);
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
			return null;
		}
		return s;
	}

	private static void print(List<String> s) {
		// TODO Auto-generated method stub

		for (String s2 : s) {
			String s3 = getLocation(s2);
			System.out.println(  s3);
		}

	}
}
