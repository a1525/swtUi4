package com.ztesoft.test.service.processer;

import java.net.URLEncoder;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.jfinal.kit.HttpKit;
import com.ztesoft.ip.entity.BaiduMap;
import com.ztesoft.ip.utils.PropertiesUtil;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 * 董伟爬虫教程：
 * 修改目标地址  修改 URL_LIST  修改URL_POST  简单修改列表xpaht代码就可以采集其他信息
 */
public class Info58Processor implements PageProcessor {

	public Integer id = 1;
	public static StringBuffer allsql = new StringBuffer("");
	public static final String URL_LIST = "http://yinchuan\\.58\\.com/jiefdj/baomu/.";

	public static final String URL_POST = "http://yinchuan\\.58\\.com/baomu/\\w+\\.shtml?\\w+";

	private Site site = Site.me().setDomain("58.com").setSleepTime(3000).setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

	@Override
	public void process(Page page) {
		// 列表页
		if (page.getUrl().regex(URL_LIST).match()) {
			// System.out.println(1);
			page.addTargetRequests(page.getHtml().xpath("//table[@id=\"jingzhun\"]").links().regex(URL_POST).all());
			page.addTargetRequests(page.getHtml().links().regex(URL_LIST).all());
			// 文章页
		} else {
			String name = page.getHtml().xpath("//div[@class='mainTitle']/h1/text()").toString();
			// System.out.println(name);
			String address = page.getHtml().xpath("//div[@class='userinfo']//ul[@class='uinfolist']//li[4]//p/text()").toString();

			// System.out.println(address);

			String localtion = getLocation(address);
			// System.out.println(localtion);
			String phone = page.getHtml().xpath("//span[@class='l_phone']/text()").get();
			// System.out.println(phone);

			makeSQL(name, address, localtion, phone);
		}
	}

	private void makeSQL(String name, String address, String localtion, String phone) {
		// TODO Auto-generated method stub
		int chanelid = 13;
		int areaid = 36;
		String sql = "insert into ccs_shop(shop_id,shop_channel_id,shop_name,area_id," + "shop_address,contact_phone,state, creator ,create_date,shop_level," + "consum_level,poi_point) values(" + (id++) + "," + chanelid + ",'" + name + "'," + areaid + ",'" + address + "','" + phone + "','A',2,to_date" + "('2015-07-08 11:46:00','yyyy-mm-dd hh24:mi:ss'),1,0.00,'" + localtion + "');";

		allsql.append(sql + "\n");

	}

	private String getLocation(String address) {
		// TODO Auto-generated method stub
		PropertiesUtil.load("resouce/config", "value.config");

		String baidu_address = "http://api.map.baidu.com/place/v2/search?ak=" + PropertiesUtil.getProperty("baiak") + "" + "&output=json&query=" + address + "&page_size=1&page_num=0&scope=1&region=" + "银川";
		// System.out.println(baidu_address);
		BaiduMap map = JSON.parseObject(HttpKit.get(baidu_address), BaiduMap.class);
		if (null == map || null == map.getResults() || map.getResults().size() < 1 || null == map.getResults().get(0).getLocation()) {
			return "未找到坐标";
		}
		return map.getResults().get(0).getLocation().getInfo();

	}

	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) {
		System.out.println("58 华新周边 保姆采集启动：");
		Spider.create(new Info58Processor()).addUrl("http://yinchuan.58.com/jiefdj/baomu/?key=月嫂&ampcmcskey=月嫂&ampfinal=1&ampspecialtype=gls&nearby=jiefdj&PGTID=163286953188173290720248650&ClickID=1").run();
		System.out.println(allsql.toString());
	}
}