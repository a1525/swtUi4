package com.ztesoft.ip.jobs;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.CharsetUtil;
import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.SecureUtil;
import com.xiaoleilu.hutool.Setting;
import com.ztesoft.ip.utils.EmailUtils;
import com.ztesoft.ip.utils.PropertiesUtil;

public class AutoSendEmail implements Runnable {
	private static final Logger log = Log.get(AutoSendEmail.class.getSimpleName());
	private static String server = null;
	private static String email = null;
	private static String passwd = null;
	private static EmailUtils eutil = new EmailUtils();

	@Override
	public void run() {

		log.info("开始发生邮件!");
		try {
			PropertiesUtil.load("resouce/config", "value.config");
			/**
			 * email_server=smtp.qq.com email_email=2642000280@qq.com
			 * email_passwd=dw222222
			 */
			this.server = PropertiesUtil.getProperty("email_server");
			this.email = PropertiesUtil.getProperty("email_email");
			this.passwd = PropertiesUtil.getProperty("email_passwd_base64");
			this.passwd = SecureUtil.decodeBase64(passwd, CharsetUtil.UTF_8);
			eutil.init(server, email, passwd);
			File f = new File("resouce/config/value.config");
			Setting s = new Setting(f, CharsetUtil.UTF_8, false);
			String[] ss = s.getStrings("getemails");
			for (String a : ss)
				System.out.println(a);
			eutil.sendSimpleEmail("310336951@qq.com", "aaa", "bbb", null);
			log.info("send success!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("send email error", e);
		}
	}
}
