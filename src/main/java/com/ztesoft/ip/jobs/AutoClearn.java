package com.ztesoft.ip.jobs;

import java.io.File;
import java.util.Date;

import org.slf4j.Logger;

import ch.qos.logback.core.util.TimeUtil;

import com.jfinal.ext.kit.DateKit;
import com.jfinal.kit.HttpKit;
import com.xiaoleilu.hutool.DateTime;
import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.Setting;

/**
 * 自动巡检应用访问情况
 * 
 * @author admin
 *
 */
public class AutoClearn implements Runnable {
	private static final Logger log = Log.get(AutoClearn.class.getSimpleName());

	@Override
	public void run() {
		log.info("开始自动清理垃圾..." + new Date().toLocaleString());

		Setting s = new Setting(new File("resouce/config/checkurl.config"), Setting.DEFAULT_CHARSET, false);
		String s0 = s.get("0", "checkurls");

		// 遍历巡检是否可以正常访问
		try {
			HttpKit.get(s0);
			HttpKit.get(s0);
			HttpKit.get(s0);
			HttpKit.get(s0);
			HttpKit.get(s0);
		} catch (Exception e) {
			// TODO: handle exception
			log.error("无法访问", e);
		}
	}
}
