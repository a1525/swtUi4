package com.ztesoft.ip.jobs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;

import com.jfinal.kit.HttpKit;
import com.xiaoleilu.hutool.CharsetUtil;
import com.xiaoleilu.hutool.DateTime;
import com.xiaoleilu.hutool.DateUtil;
import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.SecureUtil;
import com.xiaoleilu.hutool.Setting;
import com.ztesoft.ip.utils.EmailUtils;
import com.ztesoft.ip.utils.PropertiesUtil;

/**
 * 自动巡检应用访问情况
 * 
 * @author admin
 *
 */
public class AutoCheckUrls implements Runnable {
	private static final Logger log = Log.get(AutoCheckUrls.class.getSimpleName());
	List<String> canotopen = null;
	List<String> tocheck = null;

	@Override
	public void run() {

		log.info("开始自动巡检应用...");
		String server = null;
		String email = null;
		String passwd = null;
		server = PropertiesUtil.getProperty("email_server");
		email = PropertiesUtil.getProperty("email_email");
		passwd = PropertiesUtil.getProperty("email_passwd_base64");
		passwd = SecureUtil.decodeBase64(passwd, CharsetUtil.UTF_8);
		EmailUtils eutil = new EmailUtils();
		eutil.init(server, email, passwd);
		canotopen = new ArrayList<String>();
		tocheck = new ArrayList<String>();
		Setting s = new Setting(new File("resouce/config/checkurl.config"), Setting.DEFAULT_CHARSET, false);
		String s0 = s.get("0", "checkurls");
		String s1 = s.get("1", "checkurls");
		String s2 = s.get("2", "checkurls");
		String s3 = s.get("3", "checkurls");
		String s4 = s.get("4", "checkurls");
		String s5 = s.get("5", "checkurls");
		tocheck.add(s0);
		tocheck.add(s1);
		tocheck.add(s2);
		tocheck.add(s3);
		tocheck.add(s4);
		tocheck.add(s5);
		String[] s6 = s.getStrings("tell", "tellto");
		// 遍历巡检是否可以正常访问
		for (String sb : tocheck) {
			try {
				HttpKit.get(sb);
			} catch (Exception e) {
				// TODO: handle exception
				log.error("无法访问" + sb);
				canotopen.add(sb);
			}
		}
		String con = null;
		String allc = s0 + " ; " + s1 + " ; " + s2 + " ; " + s3 + " ; " + s4 + " ; " + s5;
		if (null != canotopen && canotopen.size() > 0) {
			log.info("有无法访问的系统，请及时通知管理员:数量" + canotopen.size());
			// tell emails
			con = "";
			for (String a : canotopen) {
				con = con + a + ";";
			}
			for (String a : s6) {
				try {
					eutil.sendSimpleEmail(a, "智慧社区系统巡检报告", "系统巡检完成。正在监控:" + allc + "</br>注:每小时巡检一次，系统无法正常访问才发送该报告信息!</br>" + "请及时检查:这些系统无法正常访问!</br>" + con, null);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					log.error("发送邮件失败", e);
				}
			}

		}
		// 每天8:00 19:00回报
		{
			DateTime time = new DateTime();
			int h = time.getHours();
			try {

				if (h == 8 || h == 19) {
					for (String a : s6) {
						if (null == con || con.length() < 1) {
							eutil.sendSimpleEmail(a, "智慧社区系统巡检报告", "系统巡检完成。正在监控:" + allc + "</br>注:每天8,19点发送该报告信息!</br>" + "系统正常!</br>", null);
						} else {
							eutil.sendSimpleEmail(a, "智慧社区系统巡检报告", "系统巡检完成。正在监控:" + allc + "</br>注:每天8,19点发送该报告信息!</br>" + "请及时检查:这些系统无法正常访问!</br>" + con, null);
						}
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				log.error("邮件通知失败", e);
			}
		}
	}
}
