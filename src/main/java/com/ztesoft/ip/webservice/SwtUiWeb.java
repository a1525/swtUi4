package com.ztesoft.ip.webservice;

import java.io.File;

import org.slf4j.Logger;

import com.xiaoleilu.hutool.Log;
import com.xiaoleilu.hutool.Setting;
import com.ztesoft.ip.ui.MainApp;
import com.ztesoft.ip.utils.DruidDatasource;

import ci.web.CiConfig;
import ci.web.CiService;

/**
 * 基于netty封装的简易web服务，适合java-web-api接口开发 设置脚本搜索目录 jar文件，具体看自己运行环境设置
 * 
 * @author admin
 *
 */
public class SwtUiWeb {
	private final static Logger log = Log.get(SwtUiWeb.class.getName());

	private static Setting s = new Setting(new File("resouce/config/ciweb.setting"), Setting.DEFAULT_CHARSET, false);
	private static String handlerDir = s.getString("handlerDir");
	private static String handlerPackage = s.getString("handlerPackage");
	private static Integer port = s.getInt("port");
	private static Boolean reload = s.getBool("reload");
	private static CiService service = null;

	public static void startup() {
		try {
			CiConfig config = new CiConfig();
			// config.load("resouce/config/druid.setting");
			config.port(port).handlerDir(handlerDir).handlerPackage(handlerPackage);
			service = new CiService(config);
			service.start();
			if (reload)
				service.dev();// 此方法设置后，会监控脚本 变化，实现热更新
			// 数据库连接池启动
			DruidDatasource.initDB();
		} catch (Exception e) {
			// TODO: handle exception
			log.error("SwtUiWeb", e);
			throw new RuntimeException(e);
		}
	}

	public static void stop() {
		service.stop();
		DruidDatasource.initDB();
	}

	/*
	 * public static void main(String[] args) { startup(); }
	 */
}